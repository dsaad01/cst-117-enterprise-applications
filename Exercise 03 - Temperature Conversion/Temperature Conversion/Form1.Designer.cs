﻿namespace Temperature_Conversion
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.convertCelsiusBox = new System.Windows.Forms.CheckBox();
            this.convertFahrenheitBox = new System.Windows.Forms.CheckBox();
            this.textC = new System.Windows.Forms.TextBox();
            this.textF = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.convertBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // convertCelsiusBox
            // 
            this.convertCelsiusBox.AutoSize = true;
            this.convertCelsiusBox.Location = new System.Drawing.Point(213, 95);
            this.convertCelsiusBox.Name = "convertCelsiusBox";
            this.convertCelsiusBox.Size = new System.Drawing.Size(111, 17);
            this.convertCelsiusBox.TabIndex = 0;
            this.convertCelsiusBox.Text = "Convert to Celsius";
            this.convertCelsiusBox.UseVisualStyleBackColor = true;
            // 
            // convertFahrenheitBox
            // 
            this.convertFahrenheitBox.AutoSize = true;
            this.convertFahrenheitBox.Location = new System.Drawing.Point(213, 149);
            this.convertFahrenheitBox.Name = "convertFahrenheitBox";
            this.convertFahrenheitBox.Size = new System.Drawing.Size(128, 17);
            this.convertFahrenheitBox.TabIndex = 1;
            this.convertFahrenheitBox.Text = "Convert to Fahrenheit";
            this.convertFahrenheitBox.UseVisualStyleBackColor = true;
            // 
            // textC
            // 
            this.textC.Location = new System.Drawing.Point(30, 149);
            this.textC.Name = "textC";
            this.textC.Size = new System.Drawing.Size(100, 20);
            this.textC.TabIndex = 2;
            // 
            // textF
            // 
            this.textF.Location = new System.Drawing.Point(30, 95);
            this.textF.Name = "textF";
            this.textF.Size = new System.Drawing.Size(100, 20);
            this.textF.TabIndex = 3;
            this.textF.TextChanged += new System.EventHandler(this.textF_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Celsius";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Farenheit";
            // 
            // convertBtn
            // 
            this.convertBtn.Location = new System.Drawing.Point(149, 205);
            this.convertBtn.Name = "convertBtn";
            this.convertBtn.Size = new System.Drawing.Size(75, 23);
            this.convertBtn.TabIndex = 6;
            this.convertBtn.Text = "Convert";
            this.convertBtn.UseVisualStyleBackColor = true;
            this.convertBtn.Click += new System.EventHandler(this.convertBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 277);
            this.Controls.Add(this.convertBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textF);
            this.Controls.Add(this.textC);
            this.Controls.Add(this.convertFahrenheitBox);
            this.Controls.Add(this.convertCelsiusBox);
            this.Name = "Form1";
            this.Text = "Temperature Converter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox convertCelsiusBox;
        private System.Windows.Forms.CheckBox convertFahrenheitBox;
        private System.Windows.Forms.TextBox textC;
        private System.Windows.Forms.TextBox textF;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button convertBtn;
    }
}

