﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Temperature_Conversion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Button click event
        private void convertBtn_Click(object sender, EventArgs e)
        {

            try
            {
                //Test if check box clicked Converter
                if (convertCelsiusBox.Checked == true)
                {
                    //set c = to text in Celsius then perform calculation set to Fahrenheit
                    double c = Convert.ToDouble(textC.Text);
                    //Calculation
                    c = ((c - 32) / 1.8);
                    //Set Text Fahrenheit box to format .000
                    textF.Text = c.ToString("#.000");
                }
                if (convertFahrenheitBox.Checked == true)
                {
                    //initialize and set f = to text in Fahrenheit then perform calculation set to Celsius
                    double f = Convert.ToDouble(textF.Text);
                    //Calculation
                    f = ((f - 32) / 1.8);
                    //Set Text Celsius box to format .000
                    textC.Text = f.ToString("#.000");
                }
            }
            //Catch exception rule
            catch (Exception)
            {
                MessageBox.Show("Please enter a number");
            }

        }

        private void textF_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
