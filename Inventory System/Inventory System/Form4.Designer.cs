﻿namespace Inventory_System
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.restockIDBox = new System.Windows.Forms.TextBox();
            this.restockQuantityBox = new System.Windows.Forms.TextBox();
            this.restockExecuteBTN = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(79, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter ID of Item to Restock";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(79, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(220, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Enter  Quantity of Item to Restock";
            // 
            // restockIDBox
            // 
            this.restockIDBox.Location = new System.Drawing.Point(322, 76);
            this.restockIDBox.Name = "restockIDBox";
            this.restockIDBox.Size = new System.Drawing.Size(100, 22);
            this.restockIDBox.TabIndex = 2;
            // 
            // restockQuantityBox
            // 
            this.restockQuantityBox.Location = new System.Drawing.Point(322, 138);
            this.restockQuantityBox.Name = "restockQuantityBox";
            this.restockQuantityBox.Size = new System.Drawing.Size(100, 22);
            this.restockQuantityBox.TabIndex = 3;
            // 
            // restockExecuteBTN
            // 
            this.restockExecuteBTN.Location = new System.Drawing.Point(163, 193);
            this.restockExecuteBTN.Name = "restockExecuteBTN";
            this.restockExecuteBTN.Size = new System.Drawing.Size(202, 23);
            this.restockExecuteBTN.TabIndex = 4;
            this.restockExecuteBTN.Text = "Restock  Item";
            this.restockExecuteBTN.UseVisualStyleBackColor = true;
            this.restockExecuteBTN.Click += new System.EventHandler(this.restockExecuteBTN_Click);
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 292);
            this.Controls.Add(this.restockExecuteBTN);
            this.Controls.Add(this.restockQuantityBox);
            this.Controls.Add(this.restockIDBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form4";
            this.Text = "Form4";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox restockIDBox;
        private System.Windows.Forms.TextBox restockQuantityBox;
        private System.Windows.Forms.Button restockExecuteBTN;
    }
}