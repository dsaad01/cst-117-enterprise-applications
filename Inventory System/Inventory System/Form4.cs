﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inventory_System
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void restockExecuteBTN_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Int32.Parse(restockIDBox.Text);
                int quantity = Int32.Parse(restockQuantityBox.Text);
                Inventory.Restock(id, quantity);
                this.Close();

            }
            catch (Exception)
            {
                MessageBox.Show("Please enter valid ID");
            }
        }
    }
}
