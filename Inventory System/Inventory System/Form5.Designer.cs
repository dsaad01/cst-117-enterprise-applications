﻿namespace Inventory_System
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.executeSearchBTN = new System.Windows.Forms.Button();
            this.searchIDBox = new System.Windows.Forms.TextBox();
            this.searchNameBox = new System.Windows.Forms.TextBox();
            this.searchPriceBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.searchOutputBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // executeSearchBTN
            // 
            this.executeSearchBTN.Location = new System.Drawing.Point(129, 242);
            this.executeSearchBTN.Name = "executeSearchBTN";
            this.executeSearchBTN.Size = new System.Drawing.Size(75, 23);
            this.executeSearchBTN.TabIndex = 0;
            this.executeSearchBTN.Text = "Search";
            this.executeSearchBTN.UseVisualStyleBackColor = true;
            this.executeSearchBTN.Click += new System.EventHandler(this.executeSearchBTN_Click);
            // 
            // searchIDBox
            // 
            this.searchIDBox.Location = new System.Drawing.Point(186, 75);
            this.searchIDBox.Name = "searchIDBox";
            this.searchIDBox.Size = new System.Drawing.Size(100, 22);
            this.searchIDBox.TabIndex = 1;
            this.searchIDBox.Text = "0";
            // 
            // searchNameBox
            // 
            this.searchNameBox.Location = new System.Drawing.Point(186, 132);
            this.searchNameBox.Name = "searchNameBox";
            this.searchNameBox.Size = new System.Drawing.Size(100, 22);
            this.searchNameBox.TabIndex = 2;
            this.searchNameBox.Text = "Null";
            // 
            // searchPriceBox
            // 
            this.searchPriceBox.Location = new System.Drawing.Point(186, 182);
            this.searchPriceBox.Name = "searchPriceBox";
            this.searchPriceBox.Size = new System.Drawing.Size(100, 22);
            this.searchPriceBox.TabIndex = 3;
            this.searchPriceBox.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Search by ID :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Search by Name :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(50, 187);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Search by Price :";
            // 
            // searchOutputBox
            // 
            this.searchOutputBox.Location = new System.Drawing.Point(386, 75);
            this.searchOutputBox.Multiline = true;
            this.searchOutputBox.Name = "searchOutputBox";
            this.searchOutputBox.Size = new System.Drawing.Size(345, 190);
            this.searchOutputBox.TabIndex = 8;
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 310);
            this.Controls.Add(this.searchOutputBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.searchPriceBox);
            this.Controls.Add(this.searchNameBox);
            this.Controls.Add(this.searchIDBox);
            this.Controls.Add(this.executeSearchBTN);
            this.Name = "Form5";
            this.Text = "Form5";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button executeSearchBTN;
        private System.Windows.Forms.TextBox searchIDBox;
        private System.Windows.Forms.TextBox searchNameBox;
        private System.Windows.Forms.TextBox searchPriceBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox searchOutputBox;
    }
}