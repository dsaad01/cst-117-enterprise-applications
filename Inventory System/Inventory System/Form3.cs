﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inventory_System
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void removeItemBTN_Click(object sender, EventArgs e)
        {
            try
            {
                int removeID = Int32.Parse(IDRemoveBox.Text);
                Inventory.RemoveProduct(removeID);
                this.Close();
            }
            catch (Exception)
            {

                MessageBox.Show("Please enter valid ID");
            }
        }
    }
}
