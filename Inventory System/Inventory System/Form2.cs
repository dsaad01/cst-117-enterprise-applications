﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inventory_System
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void addItemBTN_Click(object sender, EventArgs e)
        {
            try
            {
                Item add = new Item();
                add.Id = Int32.Parse(IDBox.Text);
                add.Name = nameBox.Text;
                add.Price = System.Convert.ToDecimal(priceBox.Text);
                add.Quantity = Int32.Parse(quantityBox.Text);
                Inventory.Add(add);
                this.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Please enter valid information");
            }
            
        }
    }
}
