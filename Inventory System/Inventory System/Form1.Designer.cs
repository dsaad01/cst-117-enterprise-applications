﻿namespace Inventory_System
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.executeBTN = new System.Windows.Forms.Button();
            this.outputBox = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.showInventoryBTN = new System.Windows.Forms.Button();
            this.saveBTN = new System.Windows.Forms.Button();
            this.addBTN = new System.Windows.Forms.Button();
            this.removeBTN = new System.Windows.Forms.Button();
            this.searchBTN = new System.Windows.Forms.Button();
            this.restockBTN = new System.Windows.Forms.Button();
            this.exitBTN = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // executeBTN
            // 
            this.executeBTN.Location = new System.Drawing.Point(43, 302);
            this.executeBTN.Name = "executeBTN";
            this.executeBTN.Size = new System.Drawing.Size(305, 50);
            this.executeBTN.TabIndex = 0;
            this.executeBTN.Text = "Load Inventory File";
            this.executeBTN.UseVisualStyleBackColor = true;
            this.executeBTN.Click += new System.EventHandler(this.executeBTN_Click);
            // 
            // outputBox
            // 
            this.outputBox.Location = new System.Drawing.Point(43, 12);
            this.outputBox.Multiline = true;
            this.outputBox.Name = "outputBox";
            this.outputBox.Size = new System.Drawing.Size(744, 266);
            this.outputBox.TabIndex = 1;
            this.outputBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // showInventoryBTN
            // 
            this.showInventoryBTN.Location = new System.Drawing.Point(482, 302);
            this.showInventoryBTN.Name = "showInventoryBTN";
            this.showInventoryBTN.Size = new System.Drawing.Size(305, 50);
            this.showInventoryBTN.TabIndex = 2;
            this.showInventoryBTN.Text = "Show Inventory";
            this.showInventoryBTN.UseVisualStyleBackColor = true;
            this.showInventoryBTN.Click += new System.EventHandler(this.showInventoryBTN_Click);
            // 
            // saveBTN
            // 
            this.saveBTN.Location = new System.Drawing.Point(43, 358);
            this.saveBTN.Name = "saveBTN";
            this.saveBTN.Size = new System.Drawing.Size(305, 50);
            this.saveBTN.TabIndex = 3;
            this.saveBTN.Text = "Save Inventory To File";
            this.saveBTN.UseVisualStyleBackColor = true;
            this.saveBTN.Click += new System.EventHandler(this.saveBTN_Click);
            // 
            // addBTN
            // 
            this.addBTN.Location = new System.Drawing.Point(482, 358);
            this.addBTN.Name = "addBTN";
            this.addBTN.Size = new System.Drawing.Size(305, 50);
            this.addBTN.TabIndex = 4;
            this.addBTN.Text = "Add New Item to Inventory";
            this.addBTN.UseVisualStyleBackColor = true;
            this.addBTN.Click += new System.EventHandler(this.addBTN_Click);
            // 
            // removeBTN
            // 
            this.removeBTN.Location = new System.Drawing.Point(482, 414);
            this.removeBTN.Name = "removeBTN";
            this.removeBTN.Size = new System.Drawing.Size(305, 50);
            this.removeBTN.TabIndex = 5;
            this.removeBTN.Text = "Remove an Item from Inventory";
            this.removeBTN.UseVisualStyleBackColor = true;
            this.removeBTN.Click += new System.EventHandler(this.removeBTN_Click);
            // 
            // searchBTN
            // 
            this.searchBTN.Location = new System.Drawing.Point(43, 470);
            this.searchBTN.Name = "searchBTN";
            this.searchBTN.Size = new System.Drawing.Size(305, 50);
            this.searchBTN.TabIndex = 6;
            this.searchBTN.Text = "Seach for an Item in Inventory";
            this.searchBTN.UseVisualStyleBackColor = true;
            this.searchBTN.Click += new System.EventHandler(this.searchBTN_Click);
            // 
            // restockBTN
            // 
            this.restockBTN.Location = new System.Drawing.Point(43, 414);
            this.restockBTN.Name = "restockBTN";
            this.restockBTN.Size = new System.Drawing.Size(305, 50);
            this.restockBTN.TabIndex = 7;
            this.restockBTN.Text = "Restock an Item";
            this.restockBTN.UseVisualStyleBackColor = true;
            this.restockBTN.Click += new System.EventHandler(this.restockBTN_Click);
            // 
            // exitBTN
            // 
            this.exitBTN.Location = new System.Drawing.Point(482, 470);
            this.exitBTN.Name = "exitBTN";
            this.exitBTN.Size = new System.Drawing.Size(305, 50);
            this.exitBTN.TabIndex = 8;
            this.exitBTN.Text = "Exit";
            this.exitBTN.UseVisualStyleBackColor = true;
            this.exitBTN.Click += new System.EventHandler(this.exitBTN_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 560);
            this.Controls.Add(this.exitBTN);
            this.Controls.Add(this.restockBTN);
            this.Controls.Add(this.searchBTN);
            this.Controls.Add(this.removeBTN);
            this.Controls.Add(this.addBTN);
            this.Controls.Add(this.saveBTN);
            this.Controls.Add(this.showInventoryBTN);
            this.Controls.Add(this.outputBox);
            this.Controls.Add(this.executeBTN);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button executeBTN;
        private System.Windows.Forms.TextBox outputBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button showInventoryBTN;
        private System.Windows.Forms.Button saveBTN;
        private System.Windows.Forms.Button addBTN;
        private System.Windows.Forms.Button removeBTN;
        private System.Windows.Forms.Button searchBTN;
        private System.Windows.Forms.Button restockBTN;
        private System.Windows.Forms.Button exitBTN;
    }
}

