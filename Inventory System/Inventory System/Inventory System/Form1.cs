﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inventory_System
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void executeBTN_Click(object sender, EventArgs e)
        {

        }
    }
}

[Serializable]
public class Item
{
    public int Id { get; set; }
    public string Name { get; set; }
    public decimal Price { get; set; }
    public int Quantity { get; set; }
}
public static class Inventory
{
    public static List<Item> items;
    public static List<Item> Items
    {
        get
        {
            if (items.Count == 0)
            {
                Load();
            }

            return items;
        }
        set { items = value; }
    }

    static Inventory()
    {
        Items = new List<Item>();
    }

    private static void Load()
    {
        Items = DataManager.LoadProducts();
    }

    private static void Save()
    {
        DataManager.SaveProducts(Items);
    }

    public static void RemoveProduct(int productId)
    {
        Inventory.Items.RemoveAll(x => x.Id == productId);
        Save();
    }

    public static void Add(Item item)
    {
        Items.Add(item);
        Save();
    }

    public static int GetNewId()
    {
        int id;
        if (Inventory.Items.Count == 0)
            id = 1;
        else
        {
            id = Inventory.Items.Last().Id + 1;
        }
        return id;
    }

    public static int GetProductCount()
    {
        return Inventory.Items.Count();
    }

    public static int GetUnitCount()
    {
        return Inventory.Items.Select(x => x.Quantity).Sum();
    }

    public static decimal GetInventoryValue()
    {
        return Inventory.Items.Select(x => (x.Price * x.Quantity)).Sum();
    }
    public static void ClearInventory()
    {
        Inventory.Items.Clear();
        Save();
    }
}
static class DataManager
{
    private static string dataPath = "data.json";

    public static List<Item> LoadProducts()
    {
        List<Item> listOfProducts = new List<Item>();

        if (File.Exists(dataPath))
        {
            string json = File.ReadAllText("data.json");
            if (!string.IsNullOrWhiteSpace(json))
            {
                listOfProducts = JsonConvert.DeserializeObject<List<Item>>(json);
            }
        };

        return listOfProducts;
    }

    public static void SaveProducts(List<Item> productsToSave)
    {
        if (!File.Exists(dataPath))
            File.Create(dataPath);
        string json = JsonConvert.SerializeObject(productsToSave);
        File.WriteAllText(dataPath, json);
    }
}