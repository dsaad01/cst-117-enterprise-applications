﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inventory_System
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void executeSearchBTN_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Int32.Parse(searchIDBox.Text);
                string name = searchNameBox.Text;
                decimal price = System.Convert.ToDecimal(searchPriceBox.Text);
                string output = "ID / Name / Price / Quantity" + System.Environment.NewLine;

                if (id != 0)
                {
                    foreach (var item in Inventory.Items.Where(x => x.Id == id))
                    {
                        output += item.Id.ToString() + " / ";
                        output += item.Name.ToString() + " / ";
                        output += item.Price.ToString() + " / ";
                        output += item.Quantity.ToString();
                        output += System.Environment.NewLine;
                    }
                }
                
                if (name != "Null")
                {
                    foreach (var item in Inventory.Items.Where(x => x.Name == name))
                    {
                        output += item.Id.ToString() + " / ";
                        output += item.Name.ToString() + " / ";
                        output += item.Price.ToString() + " / ";
                        output += item.Quantity.ToString();
                        output += System.Environment.NewLine;
                    }
                }
                
                if (price != 0)
                {
                    foreach (var item in Inventory.Items.Where(x => x.Price == price))
                    {
                        output += item.Id.ToString() + " / ";
                        output += item.Name.ToString() + " / ";
                        output += item.Price.ToString() + " / ";
                        output += item.Quantity.ToString();
                        output += System.Environment.NewLine;
                    }
                }

                searchOutputBox.Text = output;


            }
            catch (Exception)
            {
                MessageBox.Show("Please Check criteria");
            }
        }
    }
}
