﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inventory_System
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void executeBTN_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK) 
            {
                
                DataManager.dataPath = openFileDialog1.FileName;
                Inventory.items = DataManager.LoadProducts();

            }


            /* Testing JSON
            Item test = new Item();
            test.Id = 1;
            test.Name = "Test";
            test.Price = 10;
            test.Quantity = 5;
            Item test2 = new Item();
            test.Id = 2;
            test.Name = "Test2";
            test.Price = 5;
            test.Quantity = 2;

            Inventory.items.Add(test);
            Inventory.items.Add(test2);
            string json = JsonConvert.SerializeObject(Inventory.items);
            outputBox.Text = json;
            outputBox.Text = JsonConvert.SerializeObject(Inventory.items);*/
        }

        private void showInventoryBTN_Click(object sender, EventArgs e)
        {
            string output = "ID / Name / Price / Quantity" + System.Environment.NewLine;
            for (int i = 0; i < Inventory.items.Count; i++)
            {
                    Item item = Inventory.items[i];
                    output += item.Id.ToString() + " / ";
                    output += item.Name.ToString() + " / ";
                    output += item.Price.ToString() + " / ";
                    output += item.Quantity.ToString();
                    output += System.Environment.NewLine;
            }
            outputBox.Text = output;
        }

        private void saveBTN_Click(object sender, EventArgs e)
        {
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                DataManager.dataPath = saveFileDialog1.FileName;
                DataManager.SaveProducts(Inventory.items);
            }
        }

        private void addBTN_Click(object sender, EventArgs e)
        {
            var form2 = new Form2(); 
            form2.Show();
        }

        private void removeBTN_Click(object sender, EventArgs e)
        {
            var form3 = new Form3();
            form3.Show();
        }

        private void restockBTN_Click(object sender, EventArgs e)
        {
            var form4 = new Form4();
            form4.Show();
        }

        private void exitBTN_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.ExitThread();
        }

        private void searchBTN_Click(object sender, EventArgs e)
        {
            var form5 = new Form5();
            form5.Show();
        }
    }
}

[Serializable]
public class Item
{
    public int Id { get; set; }
    public string Name { get; set; }
    public decimal Price { get; set; }
    public int Quantity { get; set; }
}
public static class Inventory
{
    public static List<Item> items;
    public static List<Item> Items
    {
        get
        {
            if (items.Count == 0)
            {
                Load();
            }

            return items;
        }
        set { items = value; }
    }

    static Inventory()
    {
        Items = new List<Item>();
    }

    private static void Load()
    {
        Items = DataManager.LoadProducts();
    }

    private static void Save()
    {
        DataManager.SaveProducts(Items);
    }

    public static void RemoveProduct(int productId)
    {
        Inventory.Items.RemoveAll(x => x.Id == productId);
        Save();
    }

    public static void Add(Item item)
    {
        Items.Add(item);
        Save();
    }

    public static int GetNewId()
    {
        int id;
        if (Inventory.Items.Count == 0)
            id = 1;
        else
        {
            id = Inventory.Items.Last().Id + 1;
        }
        return id;
    }
    public static void Restock(int productID, int quantity)
    {
        foreach (var restorckItem in Inventory.Items.Where(x => x.Id == productID))
            restorckItem.Quantity = quantity;
    }

    public static int GetProductCount()
    {
        return Inventory.Items.Count();
    }

    public static int GetUnitCount()
    {
        return Inventory.Items.Select(x => x.Quantity).Sum();
    }

    public static decimal GetInventoryValue()
    {
        return Inventory.Items.Select(x => (x.Price * x.Quantity)).Sum();
    }
    public static void ClearInventory()
    {
        Inventory.Items.Clear();
        Save();
    }
}
static class DataManager
{
    public static string dataPath { get; set; }

    public static List<Item> LoadProducts()
    {
        List<Item> listOfProducts = new List<Item>();

        if (File.Exists(dataPath))
        {
            string json = File.ReadAllText(dataPath);
            if (!string.IsNullOrWhiteSpace(json))
            {
                listOfProducts = JsonConvert.DeserializeObject<List<Item>>(json);
            }
        };

        return listOfProducts;
    }

    public static void SaveProducts(List<Item> productsToSave)
    {
        if (!File.Exists(dataPath))
            File.Create(dataPath);
        string json = JsonConvert.SerializeObject(productsToSave);
        File.WriteAllText(dataPath, json);
    }
}
