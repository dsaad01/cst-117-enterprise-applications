﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Seconds_Counter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void secondsBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void output_TextChanged(object sender, EventArgs e)
        {

        }

        private void enter_Click(object sender, EventArgs e)
        {
            try
            {


                double c = Convert.ToDouble(secondsBox.Text);

                if (c < 60)
                {
                    MessageBox.Show("Please enter a number Greater than 60");
                }
                else if (c >= 60 && c < 3600)
                {
                    c = (c / 60);
                    output.Text = c.ToString("#.00" + " Minutes");
                }
                else if (c >= 3600 && c < 86400)
                {
                    c = (c / 3600);
                    output.Text = c.ToString("#.00" + " Hours");
                }
                else if (c >= 86400)
                {
                    c = ((c / 86400));
                    output.Text = c.ToString("#.00" + " Days");
                }
                else
                {
                    MessageBox.Show("Please enter a number Greater than 60");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Please enter a number");
            }
        }
    }
}
