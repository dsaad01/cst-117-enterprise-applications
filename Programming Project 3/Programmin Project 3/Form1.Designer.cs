﻿namespace Programmin_Project_3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.selectFileBTN = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.outputBox = new System.Windows.Forms.TextBox();
            this.saveToFileBTN = new System.Windows.Forms.Button();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(66, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select a File to Read From";
            // 
            // selectFileBTN
            // 
            this.selectFileBTN.Location = new System.Drawing.Point(225, 73);
            this.selectFileBTN.Name = "selectFileBTN";
            this.selectFileBTN.Size = new System.Drawing.Size(75, 23);
            this.selectFileBTN.TabIndex = 1;
            this.selectFileBTN.Text = "Select File";
            this.selectFileBTN.UseVisualStyleBackColor = true;
            this.selectFileBTN.Click += new System.EventHandler(this.button1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // outputBox
            // 
            this.outputBox.Location = new System.Drawing.Point(59, 102);
            this.outputBox.Multiline = true;
            this.outputBox.Name = "outputBox";
            this.outputBox.Size = new System.Drawing.Size(362, 174);
            this.outputBox.TabIndex = 2;
            // 
            // saveToFileBTN
            // 
            this.saveToFileBTN.Location = new System.Drawing.Point(200, 282);
            this.saveToFileBTN.Name = "saveToFileBTN";
            this.saveToFileBTN.Size = new System.Drawing.Size(75, 23);
            this.saveToFileBTN.TabIndex = 3;
            this.saveToFileBTN.Text = "Save to File";
            this.saveToFileBTN.UseVisualStyleBackColor = true;
            this.saveToFileBTN.Click += new System.EventHandler(this.saveToFileBTN_Click);
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 309);
            this.Controls.Add(this.saveToFileBTN);
            this.Controls.Add(this.outputBox);
            this.Controls.Add(this.selectFileBTN);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button selectFileBTN;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox outputBox;
        private System.Windows.Forms.Button saveToFileBTN;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
    }
}

