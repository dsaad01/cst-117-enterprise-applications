﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;


namespace Programmin_Project_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Method to find First word Alaphbetically
        private string FirstWord(string[] words)
        {
           Array.Sort(words, (x, y) => string.Compare(x, y));
            return words.FirstOrDefault();
        }
        //Method to find Last word Alaphbetically
        private string LastWord(string[] words)
        {
            Array.Sort(words, (x, y) => string.Compare(x, y));
            return words.LastOrDefault();
        }
        //Method to find Longest word
        private string LongestWord(string[] words)
        {
            var sorted = words.OrderBy(n => n.Length);
            return sorted.LastOrDefault();
        }
        //Method to find the word with the most vowels
        private string MostVowels(string[] words)
        {
            var sorted = words.OrderBy(n => Regex.Replace(n, "[bcdfghjklmnpqrstvwxys]", string.Empty).Length);
            return sorted.LastOrDefault();
        }

        //Read From File Button
        private void button1_Click(object sender, EventArgs e)
        {

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                using (var inputfile = new StreamReader(openFileDialog1.FileName))
                {
                    var wordsArray = inputfile.ReadToEnd().ToLower().Split(' ');
                    var first = FirstWord(wordsArray);
                    var last = LastWord(wordsArray);
                    var longest = LongestWord(wordsArray);
                    var mostVowels = MostVowels(wordsArray);
                    string newLine = Environment.NewLine;
                    outputBox.Text = ("First word is: " + first + newLine + "Last word is: " + last + newLine + "Longest word is: " + longest + newLine + 
                        "The word with the most Vowels is: " + mostVowels);
                }


            }
        }
        //Write to FIle Button
        private void saveToFileBTN_Click(object sender, EventArgs e)
        {
            if (openFileDialog2.ShowDialog() == DialogResult.OK)
            {
                TextWriter txt = new StreamWriter(openFileDialog2.FileName);
                txt.Write(outputBox.Text);
                txt.Close();
            }
        }
    }
}



