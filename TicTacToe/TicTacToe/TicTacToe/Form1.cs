﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void executeBTN_Click(object sender, EventArgs e)
        {
            int[,] gameBoard = new int[3, 3];
            int[] test = new int[3];
            bool Xwins = false;
            bool Owins = false;
            Random r = new Random();
            int n = r.Next(0, 1);
            int xcounter = 0;
            int ocounter = 0;

            for (int row = 0; row < 3; row++)
            {
                for (int column = 0; column < 3; column++)
                {
                    //Check to see if there are too many X's or O's and randomly fills if not
                    if (xcounter <= 4 && ocounter <=4)
                    {
                        n = r.Next(0, 2);
                        gameBoard[row, column] = n;
                    }
                    else if(xcounter <= 4 && ocounter > 4)
                        gameBoard[row, column] = 0;
                    else if (xcounter > 4 && ocounter <= 4)
                        gameBoard[row, column] = 1;
                    //add to X or O for tackting
                    if (n == 0)
                        xcounter++;
                    else
                        ocounter++;
                }
            }
            //Testing Rows Winner
            if (Owins == false && Xwins == false)
            {
                test = assignToTestRow(test, gameBoard, 0);
                Owins = CheckOWinner(test);
                Xwins = CheckXWinner(test);
            }

            if (Owins == false && Xwins == false)
            {
                test = assignToTestRow(test, gameBoard, 1);
                Owins = CheckOWinner(test);
                Xwins = CheckXWinner(test);
            }

            if (Owins == false && Xwins == false)
            {
                test = assignToTestRow(test, gameBoard, 2);
                Owins = CheckOWinner(test);
                Xwins = CheckXWinner(test);
            }

            //Testing Column Winner
            if (Owins == false && Xwins == false)
            {
                test = assignToTestColumn(test, gameBoard, 0);
                Owins = CheckOWinner(test);
                Xwins = CheckXWinner(test);
            }

            if (Owins == false && Xwins == false)
            {
                test = assignToTestColumn(test, gameBoard, 1);
                Owins = CheckOWinner(test);
                Xwins = CheckXWinner(test);
            }

            if (Owins == false && Xwins == false)
            {
                test = assignToTestColumn(test, gameBoard, 2);
                Owins = CheckOWinner(test);
                Xwins = CheckXWinner(test);
            }
            //Test winners diagonally
            if (Owins == false && Xwins == false)
            {
                test[0] = gameBoard[0,0];
                test[1] = gameBoard[1,1];
                test[2] = gameBoard[2,2];
                Owins = CheckOWinner(test);
                Xwins = CheckXWinner(test);
            }
            if (Owins == false && Xwins == false)
            {
                test[0] = gameBoard[0, 2];
                test[1] = gameBoard[1, 1];
                test[2] = gameBoard[2, 0];
                Owins = CheckOWinner(test);
                Xwins = CheckXWinner(test);
            }

            //Creating Game Board
            if (gameBoard[0,0] == 0)
                square00.Text = "X";
            else 
                square00.Text = "O";

            if (gameBoard[0, 1] == 0)
                square01.Text = "X";
            else 
                square01.Text = "O";

            if (gameBoard[0, 2] == 0)
                square02.Text = "X";            
            else
                square02.Text = "O";

            if (gameBoard[1, 0] == 0)
                square10.Text = "X";            
            else
                square10.Text = "O";

            if (gameBoard[1, 1] == 0)
                square11.Text = "X";
            else
                square11.Text = "O";

            if (gameBoard[1, 2] == 0)
                square12.Text = "X";
            else
                square12.Text = "O";

            if (gameBoard[2, 0] == 0)
                square20.Text = "X";
            else
                square20.Text = "O";

            if (gameBoard[2, 1] == 0)
                square21.Text = "X";
            else
                square21.Text = "O";

            if (gameBoard[2, 2] == 0)
                square22.Text = "X";
            else
                square22.Text = "O";

            
            //Set Output by winner
            if (Xwins)
            {
                outputBox.Text = "X is the Winner!";
            }
            else if (Owins)
            {
                outputBox.Text = "O is the Winner!";
            }
            else
            {
                outputBox.Text = "It's a Tie!";
            }
        }
        public bool CheckXWinner(int[] array)
        {
            if (array[0] + array[1] + array[2] == 0)
            {
                return true;
            }
            else
                return false;
            
        }
        public bool CheckOWinner(int[] array)
        {
            if (array[0] + array[1] + array[2] == 3)
            {
                return true;
            }
            else
                return false;
        }
        public int[] assignToTestRow(int[] test, int[,] game, int row)
        {
            for (int i = 0; i < 3; i++)
            {
                test[i] = game[row, i];
            }
            return test;
        }
        public int[] assignToTestColumn(int[] test, int[,] game, int column)
        {
            for (int i = 0; i < 3; i++)
            {
                test[i] = game[i, column];
            }
            return test;
        }

    }
}
