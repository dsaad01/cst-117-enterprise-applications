﻿namespace TicTacToe
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.executeBTN = new System.Windows.Forms.Button();
            this.outputBox = new System.Windows.Forms.TextBox();
            this.square00 = new System.Windows.Forms.Label();
            this.square01 = new System.Windows.Forms.Label();
            this.square02 = new System.Windows.Forms.Label();
            this.square10 = new System.Windows.Forms.Label();
            this.square11 = new System.Windows.Forms.Label();
            this.square12 = new System.Windows.Forms.Label();
            this.square20 = new System.Windows.Forms.Label();
            this.square21 = new System.Windows.Forms.Label();
            this.square22 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // executeBTN
            // 
            this.executeBTN.Location = new System.Drawing.Point(389, 405);
            this.executeBTN.Name = "executeBTN";
            this.executeBTN.Size = new System.Drawing.Size(75, 23);
            this.executeBTN.TabIndex = 0;
            this.executeBTN.Text = "Execute";
            this.executeBTN.UseVisualStyleBackColor = true;
            this.executeBTN.Click += new System.EventHandler(this.executeBTN_Click);
            // 
            // outputBox
            // 
            this.outputBox.Location = new System.Drawing.Point(241, 377);
            this.outputBox.Name = "outputBox";
            this.outputBox.Size = new System.Drawing.Size(388, 22);
            this.outputBox.TabIndex = 1;
            // 
            // square00
            // 
            this.square00.AutoSize = true;
            this.square00.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.square00.Location = new System.Drawing.Point(325, 109);
            this.square00.Name = "square00";
            this.square00.Size = new System.Drawing.Size(45, 44);
            this.square00.TabIndex = 2;
            this.square00.Text = "X";
            this.square00.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // square01
            // 
            this.square01.AutoSize = true;
            this.square01.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.square01.Location = new System.Drawing.Point(410, 109);
            this.square01.Name = "square01";
            this.square01.Size = new System.Drawing.Size(45, 44);
            this.square01.TabIndex = 3;
            this.square01.Text = "X";
            this.square01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // square02
            // 
            this.square02.AutoSize = true;
            this.square02.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.square02.Location = new System.Drawing.Point(494, 109);
            this.square02.Name = "square02";
            this.square02.Size = new System.Drawing.Size(45, 44);
            this.square02.TabIndex = 4;
            this.square02.Text = "X";
            this.square02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // square10
            // 
            this.square10.AutoSize = true;
            this.square10.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.square10.Location = new System.Drawing.Point(325, 182);
            this.square10.Name = "square10";
            this.square10.Size = new System.Drawing.Size(45, 44);
            this.square10.TabIndex = 5;
            this.square10.Text = "X";
            this.square10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // square11
            // 
            this.square11.AutoSize = true;
            this.square11.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.square11.Location = new System.Drawing.Point(410, 182);
            this.square11.Name = "square11";
            this.square11.Size = new System.Drawing.Size(45, 44);
            this.square11.TabIndex = 6;
            this.square11.Text = "X";
            this.square11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // square12
            // 
            this.square12.AutoSize = true;
            this.square12.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.square12.Location = new System.Drawing.Point(494, 182);
            this.square12.Name = "square12";
            this.square12.Size = new System.Drawing.Size(45, 44);
            this.square12.TabIndex = 7;
            this.square12.Text = "X";
            this.square12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // square20
            // 
            this.square20.AutoSize = true;
            this.square20.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.square20.Location = new System.Drawing.Point(325, 254);
            this.square20.Name = "square20";
            this.square20.Size = new System.Drawing.Size(45, 44);
            this.square20.TabIndex = 8;
            this.square20.Text = "X";
            this.square20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // square21
            // 
            this.square21.AutoSize = true;
            this.square21.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.square21.Location = new System.Drawing.Point(410, 254);
            this.square21.Name = "square21";
            this.square21.Size = new System.Drawing.Size(45, 44);
            this.square21.TabIndex = 9;
            this.square21.Text = "X";
            this.square21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // square22
            // 
            this.square22.AutoSize = true;
            this.square22.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.square22.Location = new System.Drawing.Point(494, 254);
            this.square22.Name = "square22";
            this.square22.Size = new System.Drawing.Size(45, 44);
            this.square22.TabIndex = 10;
            this.square22.Text = "X";
            this.square22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 616);
            this.Controls.Add(this.square22);
            this.Controls.Add(this.square21);
            this.Controls.Add(this.square20);
            this.Controls.Add(this.square12);
            this.Controls.Add(this.square11);
            this.Controls.Add(this.square10);
            this.Controls.Add(this.square02);
            this.Controls.Add(this.square01);
            this.Controls.Add(this.square00);
            this.Controls.Add(this.outputBox);
            this.Controls.Add(this.executeBTN);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button executeBTN;
        private System.Windows.Forms.TextBox outputBox;
        private System.Windows.Forms.Label square00;
        private System.Windows.Forms.Label square01;
        private System.Windows.Forms.Label square02;
        private System.Windows.Forms.Label square10;
        private System.Windows.Forms.Label square11;
        private System.Windows.Forms.Label square12;
        private System.Windows.Forms.Label square20;
        private System.Windows.Forms.Label square21;
        private System.Windows.Forms.Label square22;
    }
}

