﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

        // 1. Write a method that takes two integers and displays their sum.
        public int SumOfTwo(int x, int y)
        {
            return x + y;
        }
        // 2. Write a method that takes five doubles and returns their average.
        public int AvgFive(int x, int y, int z, int a, int b)
        {
            return ((x + y + z + a + b) / 5);
        }
        // 3. Write a method that returns the sum of two randomly generated integers.
        public int SumOfTwoRandom()
        {
            Random randomNumber = new Random();
            int x = randomNumber.Next(1000);
            int y = randomNumber.Next(1000);
            return x + y;
        }
        //4.	Write a method that takes three integers and returns true if their sum is divisible by 3 and false otherwise.
        public bool threeIntdisiblebyThree(int x, int y, int z)
        {
            if (((x + y + z) % 3) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        //5.	Write a method that takes two strings and displays the string that has fewer characters.
        public string compareStrings(string one, string two)
        {
            if (one.Length > two.Length)
            {
                return one;
            }
            else if (one.Length == two.Length)
            {
                return "The trings are the Same Length";
            }
            else
            {
                return two;
            }

        }
        //6.	Write a method that takes an array of doubles and returns the largest value in the array.
        public static double findLargestValue(double[] doubleArray)
        {
            double largest = doubleArray[0];
            double largestIndex = 0;

            for (int i = 0; i < doubleArray.Length; i++)
            {
                if (doubleArray[i] > largest)
                {
                    largest = doubleArray[i];
                    largestIndex = i;
                }
            }

            return largestIndex;
        }
        //7.	Write a method that generates and returns an array of fifty integer values.
        public static Array CreateArrayFifty()
        {
            int[] integers = new int[50];
            for (int i = 0; i < integers.Length; i++)
            {
                Random randomNumber = new Random();
                integers[i] = randomNumber.Next(1000);
            }
            return integers;
        }
        //8.	Write a method that takes two bool variables and return true if they have the same value and false otherwise.
        public static bool CompareTwoBoolVariables(bool x, bool y)
        {
            if (x == y)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //9.	Write a method that takes an int and a double and returns their product.
        public static double IntAndDoubleMultiply(int x, double y)
        {
            return x * y;
        }
        //10.	Write a method that takes a two-dimensional array of integers and returns the average of the entries.
        public static Array findAverageofTwoArrays(int[,] arrayOne, int[,] arrayTwo)
        {
            double[,] averageArray = new double[2,2];
            double average;
            for (int i = 0; i < 3; i++)
            {
                averageArray[i, 1] = ((arrayOne[i, 1] * arrayTwo[i, 1])/2);

            }

            for (int i = 0; i < 3; i++)
            {
                averageArray[2, i] = ((arrayOne[2, i] * arrayTwo[2, i]) / 2);

            }
            return averageArray;
            
        }
    }
}




