﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_135___Exercise_5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void executeBTN_Click(object sender, EventArgs e)
        {
            try
            {
                var piApproximate = 4.0f;
                var divisor = 1f;
                var input = int.Parse(inputBox.Text);
                
                for(var x = 1; x <= input; x++ )
                {
                    divisor += 2;
                    if (x % 2 == 0)
                    {
                        piApproximate += (4 / divisor);
                    }
                    else
                    {
                        piApproximate -= (4 / divisor);
                    }
                    Console.Write(piApproximate);
                }
                outputBox.Text = $"The approzimate value of pi after {input} is {piApproximate}";

            }
            catch (Exception)
            {
                MessageBox.Show("Please enter a integer");
            }

        }

        private void inputBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
