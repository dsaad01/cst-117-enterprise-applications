﻿namespace Exercise_8___Calories
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.executeBTN = new System.Windows.Forms.Button();
            this.FatGramsBox = new System.Windows.Forms.TextBox();
            this.CaloriesBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CarbGramsBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // executeBTN
            // 
            this.executeBTN.AccessibleRole = System.Windows.Forms.AccessibleRole.ButtonDropDown;
            this.executeBTN.Location = new System.Drawing.Point(345, 80);
            this.executeBTN.Name = "executeBTN";
            this.executeBTN.Size = new System.Drawing.Size(75, 23);
            this.executeBTN.TabIndex = 0;
            this.executeBTN.Text = "Execute";
            this.executeBTN.UseVisualStyleBackColor = true;
            this.executeBTN.Click += new System.EventHandler(this.executeBTN_Click);
            // 
            // FatGramsBox
            // 
            this.FatGramsBox.Location = new System.Drawing.Point(164, 83);
            this.FatGramsBox.Name = "FatGramsBox";
            this.FatGramsBox.Size = new System.Drawing.Size(100, 20);
            this.FatGramsBox.TabIndex = 1;
            this.FatGramsBox.Text = "0";
            // 
            // CaloriesBox
            // 
            this.CaloriesBox.Location = new System.Drawing.Point(12, 140);
            this.CaloriesBox.Name = "CaloriesBox";
            this.CaloriesBox.Size = new System.Drawing.Size(658, 20);
            this.CaloriesBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(161, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Number of Fat (Grams)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(518, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Number of Carb (Grams)";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // CarbGramsBox
            // 
            this.CarbGramsBox.Location = new System.Drawing.Point(521, 83);
            this.CarbGramsBox.Name = "CarbGramsBox";
            this.CarbGramsBox.Size = new System.Drawing.Size(100, 20);
            this.CarbGramsBox.TabIndex = 6;
            this.CarbGramsBox.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.CarbGramsBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CaloriesBox);
            this.Controls.Add(this.FatGramsBox);
            this.Controls.Add(this.executeBTN);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button executeBTN;
        private System.Windows.Forms.TextBox FatGramsBox;
        private System.Windows.Forms.TextBox CaloriesBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox CarbGramsBox;
    }
}

