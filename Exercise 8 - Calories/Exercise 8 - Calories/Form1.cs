﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_8___Calories
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void executeBTN_Click(object sender, EventArgs e)
        {
            try
            {
                var fatGrams = int.Parse(FatGramsBox.Text);
                var carbGrams = int.Parse(CarbGramsBox.Text);
                var totalCalories = 0;
                var outputText = "Calories";
                if (fatGrams != 0 && carbGrams != 0)
                {
                    totalCalories += getFatCalories(fatGrams);
                    totalCalories += getFatCalories(carbGrams);
                    outputText = "Your total Calories for " + fatGrams + " grams of fat and " + carbGrams + " grams of carbs is: " + totalCalories;
                }
                else if (fatGrams != 0)
                {
                    totalCalories += getFatCalories(fatGrams);
                    outputText = "Your total Calories for " + fatGrams + " grams of fat is: " + totalCalories;
                }
                else if (carbGrams != 0)
                {
                    totalCalories += getFatCalories(carbGrams);
                    outputText = "Your total Calories for " + carbGrams + " grams of Carbs is: " + totalCalories;
                }
                CaloriesBox.Text = outputText;

            }
            catch (Exception)
            {
                MessageBox.Show("Please enter a integer");
            }
        }
        public int getFatCalories(int numOfFatGrams)
        {
            int numOfCalories = 9 * numOfFatGrams;
            return numOfCalories;
        }
        public int getCarbCalories(int numOfCarbGrams)
        {
            int numOfCalories = 4 * numOfCarbGrams;
            return numOfCalories;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }

}
