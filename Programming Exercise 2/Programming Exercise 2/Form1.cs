﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programming_Exercise_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void checkedListBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void executeBtn_Click(object sender, EventArgs e)
        {
            double income = 25000;
            try
            {
                if (occupationBox.SelectedItem.ToString() == "Mailman")
                {
                    try
                    {
                        if (maleButton.Checked)
                        {
                            if (bsBox.Checked)
                            {
                                income = (income * 4 + 5000);
                            }
                            else if (aaBox.Checked)
                            {
                                income = (income * 3 + 5000);
                            }
                            else if (hsBox.Checked)
                            {
                                income = (income * 2 + 5000);
                            }

                        }
                        else if (femaleButton.Checked)
                        {
                            if (bsBox.Checked)
                            {
                                income = ((income * 4 + 5000) * 0.8);
                            }
                            else if (aaBox.Checked)
                            {
                                income = ((income * 3 + 5000) * 0.8);
                            }
                            else if (hsBox.Checked)
                            {
                                income = ((income * 2 + 5000) * 0.8);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Please Select a Gender");
                    }
                }
                if (occupationBox.SelectedItem.ToString() == "Nurse")
                {
                    income += 15000;
                    try
                    {
                        if(maleButton.Checked)
                        {
                            if (bsBox.Checked)
                            {
                                income = (income * 4 + 7000);
                            }
                            else if (aaBox.Checked)
                            {
                                income = (income * 5 + 7000);
                            }
                            else if (hsBox.Checked)
                            {
                                income = (income * 6 + 7000);
                            }

                        }
                        else if (femaleButton.Checked)
                        {
                            if (bsBox.Checked)
                            {
                                income = ((income * 5 + 7000) * 0.8);
                            }
                            else if (aaBox.Checked)
                            {
                                income = ((income * 6 + 7000) * 0.8);
                            }
                            else if (hsBox.Checked)
                            {
                                income = ((income * 7 + 7000) * 0.8);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Please Select a Gender");
                    }
                }
                if (occupationBox.SelectedItem.ToString() == "Programmer")
                {
                    income += 13000;
                    try
                    {
                        if (maleButton.Checked)
                        {
                            if (bsBox.Checked)
                            {
                                income = (income * 7 + 10000);
                            }
                            else if (aaBox.Checked)
                            {
                                income = (income * 8 + 10000);
                            }
                            else if (hsBox.Checked)
                            {
                                income = (income * 9 + 10000);
                            }

                        }
                        else if (femaleButton.Checked)
                        {
                            if (bsBox.Checked)
                            {
                                income = ((income * 7 + 10000) * 0.8);
                            }
                            else if (aaBox.Checked)
                            {
                                income = ((income * 8 + 10000) * 0.8);
                            }
                            else if (hsBox.Checked)
                            {
                                income = ((income * 9 + 10000) * 0.8);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Please Select a Gender");
                    }
                }
                if (occupationBox.SelectedItem.ToString() == "Rock Star")
                {
                    income += 25000;
                    try
                    {
                        if (maleButton.Checked)
                        {
                            if (bsBox.Checked)
                            {
                                income = (income * 8 + 20000);
                            }
                            else if (aaBox.Checked)
                            {
                                income = (income * 10 + 20000);
                            }
                            else if (hsBox.Checked)
                            {
                                income = (income * 14 + 20000);
                            }

                        }
                        else if (femaleButton.Checked)
                        {
                            if (bsBox.Checked)
                            {
                                income = ((income * 7 + 20000) * 0.8);
                            }
                            else if (aaBox.Checked)
                            {
                                income = ((income * 8 + 20000) * 0.8);
                            }
                            else if (hsBox.Checked)
                            {
                                income = ((income * 9 + 20000) * 0.8);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Please Select a Gender");
                    }
                }


                incomeBox.Text = income.ToString() + " / year";
            }
            catch (Exception)
            {
                MessageBox.Show("Please Select a Occupation");
            }
        }
    }
}
