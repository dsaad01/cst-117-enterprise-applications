﻿namespace Programming_Exercise_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.maleButton = new System.Windows.Forms.RadioButton();
            this.femaleButton = new System.Windows.Forms.RadioButton();
            this.hsBox = new System.Windows.Forms.CheckBox();
            this.aaBox = new System.Windows.Forms.CheckBox();
            this.occupationBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.incomeBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bsBox = new System.Windows.Forms.CheckBox();
            this.executeBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // maleButton
            // 
            this.maleButton.AutoSize = true;
            this.maleButton.Location = new System.Drawing.Point(65, 161);
            this.maleButton.Name = "maleButton";
            this.maleButton.Size = new System.Drawing.Size(48, 17);
            this.maleButton.TabIndex = 1;
            this.maleButton.TabStop = true;
            this.maleButton.Text = "Male";
            this.maleButton.UseVisualStyleBackColor = true;
            // 
            // femaleButton
            // 
            this.femaleButton.AutoSize = true;
            this.femaleButton.Location = new System.Drawing.Point(65, 184);
            this.femaleButton.Name = "femaleButton";
            this.femaleButton.Size = new System.Drawing.Size(59, 17);
            this.femaleButton.TabIndex = 2;
            this.femaleButton.TabStop = true;
            this.femaleButton.Text = "Female";
            this.femaleButton.UseVisualStyleBackColor = true;
            // 
            // hsBox
            // 
            this.hsBox.AutoSize = true;
            this.hsBox.Location = new System.Drawing.Point(65, 239);
            this.hsBox.Name = "hsBox";
            this.hsBox.Size = new System.Drawing.Size(158, 17);
            this.hsBox.TabIndex = 4;
            this.hsBox.Text = "GED or Highschool Diploma";
            this.hsBox.UseVisualStyleBackColor = true;
            // 
            // aaBox
            // 
            this.aaBox.AutoSize = true;
            this.aaBox.Location = new System.Drawing.Point(65, 262);
            this.aaBox.Name = "aaBox";
            this.aaBox.Size = new System.Drawing.Size(116, 17);
            this.aaBox.TabIndex = 5;
            this.aaBox.Text = "Accosiates Degree";
            this.aaBox.UseVisualStyleBackColor = true;
            // 
            // occupationBox
            // 
            this.occupationBox.FormattingEnabled = true;
            this.occupationBox.Items.AddRange(new object[] {
            "Mailman",
            "Nurse",
            "Programmer",
            "Rock Star"});
            this.occupationBox.Location = new System.Drawing.Point(65, 78);
            this.occupationBox.Name = "occupationBox";
            this.occupationBox.Size = new System.Drawing.Size(124, 56);
            this.occupationBox.TabIndex = 7;
            this.occupationBox.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Select Occupation";
            // 
            // incomeBox
            // 
            this.incomeBox.Location = new System.Drawing.Point(340, 171);
            this.incomeBox.Name = "incomeBox";
            this.incomeBox.Size = new System.Drawing.Size(100, 20);
            this.incomeBox.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(337, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Estimated Income";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Select Gender";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(62, 214);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Select all that apply";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // bsBox
            // 
            this.bsBox.AutoSize = true;
            this.bsBox.Location = new System.Drawing.Point(65, 285);
            this.bsBox.Name = "bsBox";
            this.bsBox.Size = new System.Drawing.Size(113, 17);
            this.bsBox.TabIndex = 13;
            this.bsBox.Text = "Bachelor\'s Degree";
            this.bsBox.UseVisualStyleBackColor = true;
            // 
            // executeBtn
            // 
            this.executeBtn.Location = new System.Drawing.Point(353, 249);
            this.executeBtn.Name = "executeBtn";
            this.executeBtn.Size = new System.Drawing.Size(75, 23);
            this.executeBtn.TabIndex = 14;
            this.executeBtn.Text = "Execute";
            this.executeBtn.UseVisualStyleBackColor = true;
            this.executeBtn.Click += new System.EventHandler(this.executeBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 353);
            this.Controls.Add(this.executeBtn);
            this.Controls.Add(this.bsBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.incomeBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.occupationBox);
            this.Controls.Add(this.aaBox);
            this.Controls.Add(this.hsBox);
            this.Controls.Add(this.femaleButton);
            this.Controls.Add(this.maleButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RadioButton maleButton;
        private System.Windows.Forms.RadioButton femaleButton;
        private System.Windows.Forms.CheckBox hsBox;
        private System.Windows.Forms.CheckBox aaBox;
        private System.Windows.Forms.ListBox occupationBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox incomeBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox bsBox;
        private System.Windows.Forms.Button executeBtn;
    }
}

